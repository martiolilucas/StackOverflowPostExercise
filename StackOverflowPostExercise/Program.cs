﻿using System;

namespace StackOverflowPostExercise
{
    class Program
    {
        static void Main(string[] args)
        {

            var post = new StackOverflowPost("Testing stackoverflowpost class", "Do this class doing well?");

            post.UpVote();
            post.UpVote();
            post.UpVote();
            post.UpVote();
            post.UpVote();
            post.UpVote();
            post.UpVote();
            post.DownVote();
            post.DownVote();


            Console.WriteLine(post.Title);
            Console.WriteLine(post.Description);
            Console.WriteLine("Up votes: {0}  Down votes: {1}", post.UpVotes, post.DownVotes);
        }
    }
}
