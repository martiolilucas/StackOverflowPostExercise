﻿using System;

namespace StackOverflowPostExercise
{
    public class StackOverflowPost
    {
        private readonly DateTime _createDateTimePost;
        private readonly string _title;
        private readonly string _description;

        public StackOverflowPost(string title, string description)
        {
            _title = title;
            _description = description;
            _createDateTimePost = DateTime.Now;
            UpVotes = 0;
            DownVotes = 0;
        }


        public int UpVotes { private set; get; }
        public int DownVotes { private set; get; }
        public DateTime CreateDateTimePost => _createDateTimePost;
        public string Title => _title;
        public string Description => _description;

        public void UpVote()
        {
            UpVotes++;
        }

        public void DownVote()
        {
            DownVotes++;
        }
    }
}